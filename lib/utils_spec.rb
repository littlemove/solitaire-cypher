require './utils.rb'

describe Utils do

  it "Sanitizes text" do
    expect(Utils.sanitize("Code in Ruby, live longer!")).to eq("CODEI NRUBY LIVEL ONGER")
  end

  it "Encodes text" do
    encoded_text = Utils.encode("Code in Ruby, live longer!")
    expect(encoded_text).to eq("3 15 4 5 9  14 18 21 2 25  12 9 22 5 12  15 14 7 5 18")
  end

  it "Mixes encoded text" do
    str1, str2 =
          '4 23 10 24 8 25 18 6 4 7 20 13 19 8 16 21 21 18 24 10',
    '3 15 4 5 9  14 18 21 2 25  12 9 22 5 12  15 14 7 5 18'

    result = Utils.mix(str1, str2)
    expect(result).to eq('7 12 14 3 17 13 10 1 6 6 6 22 15 13 2 10 9 25 3 2')
  end

  it "Transform numbers to text" do
    str = '7 12 14 3 17 13 10 1 6 6 6 22 15 13 2 10 9 25 3 2'

    result = Utils.decode(str)
    expect(result).to eq('GLNCQ MJAFF FVOMB JIYCB')
  end
end
