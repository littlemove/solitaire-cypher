module Utils
  extend self

  def sanitize(text)
    text.upcase.gsub(/[^A-Z]/,'').gsub(/\w{5}/) { |s| "#{s} " }.rstrip
  end

  def encode(text)
    numeric_string = ''
    sanitize(text).each_byte { |c| numeric_string << "#{c-64} "}
    numeric_string.gsub('-32','').rstrip
  end

  def decode(text)
    letters = ('A'..'Z').to_a
    str =
      text.split(' ').map { |c| letters[c.to_i - 1] }
    str.join().gsub(/\w{5}/) { |s| "#{s} " }.rstrip
  end

  def mix(str1, str2)
    result = []
    str1.split(' ').each_with_index do |n, i|
      temp = (str2.split(' ')[i].to_i + n.to_i)
      result << (temp > 26 ? temp - 26 : temp)
    end
    result.join(' ')
  end

end
