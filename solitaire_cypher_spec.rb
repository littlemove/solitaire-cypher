require "./solitaire_cypher"

describe SolitaireCypher do

  it "does the 3d text transformation" do
    sc = SolitaireCypher.new("Code in Ruby, live longer!")
    expect(sc.encode).to eq("3 15 4 5 9  14 18 21 2 25  12 9 22 5 12  15 14 7 5 18")
  end

  it "does the 4th text transformation" do
    sc = SolitaireCypher.new
    str_from_4th_step = '4 23 10 24 8 25 18 6 4 7 20 13 19 8 16 21 21 18 24 10'
    str_from_3d_step = '3 15 4 5 9  14 18 21 2 25  12 9 22 5 12  15 14 7 5 18'
    expect(sc.fourth_step(str_from_3d_step, str_from_4th_step)).to eq('7 12 14 3 17 13 10 1 6 6 6 22 15 13 2 10 9 25 3 2')
  end

end
